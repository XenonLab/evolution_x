# Evolution_X

This project was born with the intention of continuing to improve the world of FSX.
I really like to try to bring the maximum realism possible without compromising the performance and the game features.
In the hope that other passionate simmer will appreciate my improvements!

## Credits:

###### Founder of the project:

* Stefano Peris

###### Programmer:

* Stefano Peris
* Dirk Kurze

###### Testing and optimization of contents:

* Stefano Peris

###### Textures editing:

* Stefano Peris
* Jason A. Lee
* Enrique Cornejo


## Changelog (05/02/2019)

## Update for A321:

* Up-to-date textures for the cockpit with better resolution and more colors and details.

* The information texts have been modified to match the new engines.

* An explanation on how to disable flight controls for better controllability.

1. Fix for autobrake, this correction works only for the cockpit 2D panel, the VC buttons do not work yet, but we are working to implement them in the Virtual Cockpit too.
2. A new soundset for the A321 recorded by real V2500 engines.
3. Updated sounds for a better flight experience.
4. Removed some unwanted ticks in the sound during the cruise flight.
5. Missing warning sounds have been added.
6. The missing autopilot sounds have been added.
7. Added new touchdowns and roll sounds.


* I moved some positions in the VC for better control and I updated the aircraft.cfg file with optimized values. 

Improved flight dynamics:

1. Improved take-off length and landing speed. Now you will find it much easier to control because the acceleration is reduced and no longer has the characteristics of a military jet. The takeoff now requires a greater distance of the runway and appears decidedly more realistic according to the real mass of the plane.
2. The pitching during the ascent / descent phase has been reduced (it tended to stall the plane because of the too high default value). This was done by entering the data of the real plane.
3. It now implements V2500 engines instead of the CFM-56 engines.
4. A321 no longer has critical bugs that compromised its use. The values relating to the structural masses and the real thrust of the engines have been corrected to have an A321 finally realistic in flight behavior.
5. Cut to more realistic touchdown.
6. 'Nose-down' effect slightly reduced.
7. Improved inertia.

 
**IMPORTANT:**

Remember that the A321 is not yet perfect and probably never will be in FSX. We are working to achieve a certain balance to make this aircraft more realistic and fun to use. The following additions and corrections in the A321 are still missing: 

1. Implementation of the autobrake in the Virtual Cockpit.
2. Further optimization of the flight model.
3. New textures for the cockpit definitely less poor in details (the new textures in this version of Evolution X are much better than the original).
4. Revisions and various corrections. Unfortunately, the A321 is the plane with major faults in FSX, but with Evolution X it has improved considerably.


## Update for Boeing 737-800:

* Up-to-date textures for the cockpit with better resolution and more colors and details.

* Resolves several problems with predefined indicators. Includes PFD Flight Director correction.

* The LE buzzer lights now flash correctly (they did not work by default at all).

* The vertical (rolling) needle of the Flight Director now moves correctly.

* Landing lights turn on the overhead that now works correctly (it worked in reverse).

* The logo lights turn on the "switch nav lights" lock (there was no default nav light switch).

* The fuel supply lamp on the ceiling now does not dim at night with the lights on the panel turned off.


## Update for Boeing 747-400:

* Up-to-date textures for the cockpit with better resolution and more colors and details.

* BC button moved to the VS coordinates, (the coordinates of the original BC button: 256, 64, 29, 30).

* the autopilot master switch (CMD), moved.

* The new enabled buttons will appear just above the PFD (Primary Flight Display).

* Some of these indicators / buttons are inside "Beech_Baron.cab". This file is already present in: "Microsoft Flight Simulator X Steam Edition \ gauges" and the indicators will be found there automatically.

* It is not necessary to add "Beech_Baron.cab" to the 747_400 folder.

* added, B747_400 full-exp switch, on top of PFD.

* added Beech_Baron pitot heat sw, on top of the PFD.

* added Beech_Baron Battery Switch, on top of the PFD.
 
* added the Kneeboard icon, on top of the PFD.


## Textures improvements:

* New detailed and optimized textures for the tracks (they are applied in all the airports of the world):

* taxi_asphalt

* taxi_tranm

* taxi_concrete

* taxi_ asphalt

* taxiway_marks

* Added new sky textures with 32-bit color depth, for optimal use on FSX. The effect is greatly improved making the setting much more realistic, without weighing the performance of the graphics engine.

* Added new textures for the trees, with different shades according to the seasons and the climatic conditions.

* Added new textures for the streets. Now the streets of the whole world will appear much more realistic.

* I designed new buildings, trees, and finally I continued to build photorealistic roads and railways.

* New textures for houses and structures in the gaming world. These new textures improve cities, towns, airports, and during night flights they reflect the light more, giving greater realism and spectacularity to the whole scenario.


## Lighting improvement:

* Adds more realistic light colors with better remote viewing. Together with the new textures added now the lights will appear much more spectacular, greatly increasing the visibility of airports and air traffic in night flights.

* The sun is more radiant and its splendor lasts longer after sunset. Improves the reflection on the surfaces of objects and aircraft.

* Adds a special realistic xenon light for all aircraft, offering a much better view of the runway and taxiways. VFR night flights will be much more spectacular and realistic without weighing the performance of the graphics engine.

* Improve all sources of lighting with the application of the "Bloom" effect that can be activated from the game settings menu.

* Improve performance with the use of DirectX10 (activated from the game menu).


## Improvement scenarios:

* Flight Simulator X uses the "terrain.cfg" file to define the placement of textures in the terrain. During the tests, I've applied a number of changes to this file that fix some problems with the world of FSX, and will also provide more options for scenario designers. For example, in some places, water areas showed rocky areas.

* Changes and improvements to the regions of Italy (including cities and countries.) Corrects some bugs and now Italy is no longer a desert country (Fiumicino-Malpensa seemed to fly over Israel) I corrected the coordinates of the textures in the various regions and the annoying "proliferate" of houses in places where there should not be!


# License:

## Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License



